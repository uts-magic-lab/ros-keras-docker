FROM tensorflow/tensorflow:1.7.0-gpu

##########
# ROS-core
##########

# install packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    dirmngr \
    gnupg2 \
    && rm -rf /var/lib/apt/lists/*

# setup keys
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 421C365BD9FF1F717815A3895523BAEEB01FA116

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu xenial main" > /etc/apt/sources.list.d/ros-latest.list

# install ros packages
ENV ROS_DISTRO kinetic
RUN apt-get update && apt-get install -y \
    ros-kinetic-desktop-full \
    && rm -rf /var/lib/apt/lists/*

# bootstrap rosdep
RUN rosdep init \
    && rosdep update

# Add ROS sourcing by default
RUN echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

RUN . /opt/ros/kinetic/setup.sh && rospack profile


# install bootstrap tools
RUN apt-get update && apt-get install -y \
    python-rosinstall python-rosinstall-generator python-wstool build-essential \
    && rm -rf /var/lib/apt/lists/*

# setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN . /opt/ros/kinetic/setup.sh && rospack profile

# # install ros packages
# ENV ROS_DISTRO kinetic
# RUN apt-get update && apt-get install -y \
#     ros-kinetic-ros-core \
#     && rm -rf /var/lib/apt/lists/*

# ##########
# # ROS-base
# ##########

# RUN apt-get update && apt-get install -y \
#     ros-kinetic-ros-base \
#     && rm -rf /var/lib/apt/lists/*

# ##########
# # ROS-perception
# ##########

# RUN apt-get update && apt-get install -y \
#     ros-kinetic-perception \
#     && rm -rf /var/lib/apt/lists/*


##########
# ROS-magiclab
##########

# Install debian packages
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    nano \
    openssh-client \
    openvpn \
    python-tk \
    ros-kinetic-naoqi-bridge-msgs \
    ros-kinetic-navigation \
    ros-kinetic-rosbridge-suite \
    ros-kinetic-smach-ros \
    ros-kinetic-web-video-server \
    libsndfile1 \
    sudo && \
rm -rf /var/lib/apt/lists/*

RUN . /opt/ros/kinetic/setup.sh && rospack profile

# Install pip
RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
python get-pip.py && \
rm get-pip.py

# Install pip packages
RUN pip install --upgrade \
    'ipython[notebook]<=6.0' \
    pyOpenSSL \
    'requests[security]' \
    scikit-image \
    scipy \
    setuptools \
    'tornado<5.0' \
    websocket-client \
    Watchdog

RUN . /opt/ros/kinetic/setup.sh && rospack profile

##########
# Keras upgrade
##########
RUN pip install --upgrade tensorflow
RUN pip install --upgrade git+https://github.com/Theano/Theano.git#egg=Theano
RUN pip install --upgrade keras

# Create nonprivileged user to run rosdep
RUN useradd --create-home --shell=/bin/bash rosuser
RUN sudo -u rosuser -H rosdep update

# Install pynaoqi and fix permissions
RUN cd /usr/local && \
# softbank server is down, workaround
#curl -s 'https://developer.softbankrobotics.com/Software/Python/2.5.5/Linux/pynaoqi-python2.7-2.5.5.5-linux64.tar.gz' \
curl -s http://192.168.1.8/pepper_os/pynaoqi-python2.7-2.5.5.5-linux64.tar.gz \
| tar xz && \
cd /usr/local/pynaoqi-python2.7-2.5.5.5-linux64 && \
chmod -R +r . && \
find . -type d -exec chmod +x {} \;

# Add scripts from this package
ADD assets /
RUN chown -R rosuser /home/rosuser/
RUN chmod -R o-w /etc/sudoers.d/

# Install pyGPU
RUN pip install Cython
RUN git clone https://github.com/Theano/libgpuarray.git && \
    cd libgpuarray  && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local && \
    make && make install && \
    cd .. && \
    python setup.py build && \
    python setup.py install

RUN . /opt/ros/kinetic/setup.sh && rospack profile

# Install extra useful stuff
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ros-kinetic-rqt* \
    net-tools \
    less \
    gedit \
    meld \
    sudo && \
rm -rf /var/lib/apt/lists/*

RUN . /opt/ros/kinetic/setup.sh && rospack profile
RUN . /opt/ros/kinetic/setup.sh && echo $PATH
RUN . /opt/ros/kinetic/setup.sh && echo $LD_LIBRARY_PATH

# Adjust flags and paths
ENV KERAS_BACKEND=theano
ENV THEANO_FLAGS=device=cuda
ENV GPUARRAY_TEST_DEVICE=cuda

# Interactive niceties
ENV TERM xterm-color
ENV EDITOR nano -wi

# Publish roscore port
EXPOSE 11311 9090 8080 8888

ENTRYPOINT ["/usr/local/bin/ros_entrypoint"]
