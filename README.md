# ros-keras

GPU Docker image with tensorflow, theano, keras, ros, and pynaoqi installed.

### Development

    docker build -t magiclab/ros-keras .
    docker push magiclab/ros-keras

### Usage

    nvidia-docker run magiclab/ros-keras

If you want to:
 * Be able to run graphical applications (`xhost` command + `-e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw`).
 * Have the same network as the host (`--net host`).
 * Have the same rights on devices (`--privileged`).

```bash
# Mandatory before
xhost +local:root;
# Now actually run it
nvidia-docker run --net host --privileged -v $HOME:/shared -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw -it --rm magiclab/ros-keras
```

