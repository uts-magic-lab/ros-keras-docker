case ":$PATH:" in
    *:"$HOME/.local/bin":*) :;; # already there
    *) PATH="$HOME/.local/bin:$PATH" ;;
esac
